﻿namespace FB2
{
    public class Attribute
    {
        public string CustomNamespace { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return string.IsNullOrEmpty(CustomNamespace) ? 
                $" {Name}=\"{Value}\"" : 
                $" {CustomNamespace}:{Name}=\"{Value}\"";
        }
    }
}
