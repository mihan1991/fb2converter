﻿namespace FB2
{
    public class Constants
    {
        public const string Xmlns = "http://www.gribuser.ru/xml/fictionbook/2.0";
        public const string Xlinkns = "http://www.w3.org/1999/xlink";
        public const string XlinkName = "xlink";
    }
}
