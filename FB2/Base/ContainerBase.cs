﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public abstract class ContainerBase : ElementBase, IContainer
    {
        public IEnumerable<IItem> Items { get; set; }

        public ContainerBase()
        {
            Items = new List<IElement>();
        }

        public void AddItem(IElement item)
        {
            AddItems(new[] { item });
        }

        public void AddItems(IEnumerable<IElement> items)
        {
            Items = Items.Concat(items);
        }        

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();            
            foreach (var item in Items)
                sb.Append(item);
            return Wrap(sb.ToString());
        }
    }
}