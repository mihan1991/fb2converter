﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public abstract class ElementBase : IElement
    {
        public IEnumerable<Attribute> Attributes { get; set; }

        public abstract string ElemName { get; }

        public ElementBase()
        {
            Attributes = new List<Attribute>();
        }

        public void AddAttribute(Attribute attr)
        {
            AddAttributes(new[] { attr });
        }

        public void AddAttributes(IEnumerable<Attribute> attrs)
        {
            Attributes = Attributes.Concat(attrs);
        }

        protected string Wrap(string content)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"<{ElemName}");
            foreach (var a in Attributes)
                sb.Append(a);
            sb.Append(string.IsNullOrEmpty(content) ?
                " />" :
                $">{content}</{ElemName}>");
            return sb.ToString();
        }
    }
}
