﻿using System.Threading.Tasks;

namespace FB2
{
    public interface IBuilder
    {
        Task BuildAsync();
    }
}
