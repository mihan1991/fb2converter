﻿using System.Collections.Generic;

namespace FB2
{
    public interface IContainer : IElement
    {
        IEnumerable<IItem> Items { get; set; }
    }
}
