﻿using System.Collections.Generic;

namespace FB2
{
    public interface IElement : IItem
    {
        IEnumerable<Attribute> Attributes { get; set; }
        string ElemName { get; }
    }
}
