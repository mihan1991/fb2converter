﻿namespace FB2
{
    public interface ITextual : IItem
    {
        string Text { get; set; }
    }
}