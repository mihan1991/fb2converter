﻿namespace FB2
{
    public interface IVerifiable
    {
        bool Verified();
    }
}
