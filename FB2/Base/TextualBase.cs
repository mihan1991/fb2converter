﻿namespace FB2
{
    public abstract class TextualBase : ElementBase, ITextual
    {
        public string Text { get; set; }

        public override string ToString() => Wrap(Text);
    }
}
