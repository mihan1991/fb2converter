﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public abstract class TextualContainerBase : TextualBase, IContainer
    {
        public IEnumerable<IItem> Items { get; set; }

        public new string Text 
        { 
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (var item in Items)
                    sb.Append(item);
                return sb.ToString();
            }
        }

        public TextualContainerBase()
        {
            Items = new List<ITextual>();
        }

        public void AddItem(ITextual item)
        {
            AddItems(new[] { item });
        }

        public void AddItems(IEnumerable<ITextual> items)
        {
            Items.Concat(items);
        }

        public override string ToString()
        {
            return Wrap(Text);
        }
    }
}