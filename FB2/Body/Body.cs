﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public class Body : ContainerBase, IVerifiable
    {
        // TODO: add Image

        // public Image Image = new Image();
        public Title Title { get; set; }
        public IEnumerable<Epigraph> Epigraphs { get; set; }
        public IEnumerable<Section> Sections { get; set; }
        public override string ElemName => "body";

        public Body()
        {
            Epigraphs= new List<Epigraph>();
            Sections= new List<Section>();
        }

        public bool Verified()
        {
            return Sections.Count() > 0;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least one 'Section'.");

            StringBuilder sb = new StringBuilder();
            // sb.Append(Image);
            sb.Append(Title);
            foreach (var e in Epigraphs)
                sb.Append(e);
            foreach (var s in Sections)
                sb.Append(s);
            return Wrap(sb.ToString());
        }
    }
}
