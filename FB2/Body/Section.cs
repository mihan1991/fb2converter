﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public class Section : ContainerBase, IVerifiable
    {
        // TODO: add Image

        public Title Title { get; set; }
        public IEnumerable<Epigraph> Epigraphs { get; set; }
        // public Image Image { get; set; }
        public Annotation Annotation { get; set; }

        /// <summary>
        /// Delete unnecessary Items if there are Sections among them. 
        /// </summary>
        public bool Autofix { get; set; }
        public override string ElemName => "section";

        public Section()
        {
            Epigraphs = new List<Epigraph>();
        }

        public bool ContainsSections => Items.Where(i => i is Section)
                                             .Count() > 0;

        public bool Verified()
        {
            int sectionsCount = Items.Where(i => i is Section)
                                     .Count();
            return sectionsCount == Items.Count() || sectionsCount == 0;
        }

        public override string ToString()
        {
            if(Autofix && ContainsSections)
            {
                Items = Items.Where(i => i is Section);
            }

            if (!Verified())
                throw new Exception($"{GetType().Name} can contain either a set of 'Section' or set of other containers.");
            
            StringBuilder sb = new StringBuilder();
            sb.Append(Title);
            foreach(var e in Epigraphs)
                sb.Append(e);
            // sb.Append(Image);
            sb.Append(Annotation);
            foreach(var i in Items)
                sb.Append(i);

            return Wrap(sb.ToString());
        }
    }
}
