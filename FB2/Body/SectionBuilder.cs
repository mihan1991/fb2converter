﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FB2
{
    public class SectionBuilder : IBuilder
    {
        private string[] text;
        private Section section;
        private int begin;
        private int end;

        public SectionBuilder(IEnumerable<string> text, Section section, int begin, int end)
        {
            this.text = text.ToArray();
            this.section = section;
            this.begin = begin;
            this.end = end;
        }

        public async Task BuildAsync()
        {
            var tasks = new List<Task<IElement>>
            {
                CreateTitleAsync(text[begin])
            };

            if (!section.ContainsSections)
            {
                int s = begin + 1;
                int e = s;

                for (int i = begin + 1; i <= end; i++)
                {
                    if (string.IsNullOrEmpty(text[i]))
                        tasks.Add(CreateEmptyLineAsync());

                    // if start with two or more spaces make Cite
                    else if (text[i].StartsWith("  "))
                    {
                        s = i;
                        e = i;
                        while (e + 1 <= end && text[e + 1].StartsWith("  "))
                            e++;
                        tasks.Add(CreateCiteAsync(s, e));
                        i = e;
                    }

                    // if start with one space make simple P
                    else if (text[i].StartsWith(' '))
                    {
                        s = i;
                        e = i;
                        while (e + 1 <= end && (!text[e + 1].StartsWith(' ') || string.IsNullOrEmpty(text[e + 1])))
                            e++;
                        tasks.Add(CreateParagraphAsync(s, e));
                        i = e;
                    }
                }
            }

            await Task.WhenAll(tasks);

            section.Title = tasks[0].Result as Title;
            for (int i = 1; i < tasks.Count; i++)
                section.AddItem(tasks[i].Result);
        }

        private async Task<IElement> CreateTitleAsync(string title) 
        {
            var strTitle = title.Trim().Split('.');

            var paragraphs = new List<P>(strTitle.Length);

            foreach (var s in strTitle)
            {
                paragraphs.Add(new P
                {
                    Items = new List<ITextual>
                    {
                        new TextString { Text = s.Trim() }
                    }
                });
            }

            return new Title { Items = paragraphs };
        }

        private async Task<IElement> CreateParagraphAsync(int start, int end)
        {
            StringBuilder sb = new StringBuilder();
            for(int i = start; i<=end; i++)
                sb.Append($" {text[i].Trim()}");
            return new P
            {                
                Items = new List<ITextual>
                {
                    new TextString { Text = sb.ToString().Trim() }
                }
            };
        }

        private async Task<IElement> CreateEmptyLineAsync() => new EmptyLine();

        private async Task<IElement> CreateCiteAsync(int start, int end)
        {
            var paragraphs = new List<P>();
            for (int i = start; i <= end; i++)
                paragraphs.Add(new P
                {
                    Items = new List<ITextual>
                    {
                        new TextString { Text = text[i].Trim() }
                    }
                });
            return new Cite
            {
                Items = paragraphs
            };
        }
    }
}
