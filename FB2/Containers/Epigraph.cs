﻿using System.Collections.Generic;
using System.Text;

namespace FB2
{
    public class Epigraph : ContainerBase
    {
        public IEnumerable<TextAuthor> TextAuthors { get; set; }
        public override string ElemName => "epigraph";

        public Epigraph() 
        {
            TextAuthors= new List<TextAuthor>();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var i in Items)
                sb.Append(i);
            foreach (var t in TextAuthors)
                sb.Append(t);

            return Wrap(sb.ToString());
        }
    }
}