﻿using System;

namespace FB2
{
    public abstract class PersonBase : ContainerBase, IVerifiable
    {
        public FirstName FirstName { get; set; }
        public MiddleName MiddleName { get; set; }
        public LastName LastName { get; set; }
        public Nickname Nickname { get; set; }
        public Homepage Homepage { get; set; }
        public Email Email { get; set; }
        public Id Id { get; set; }

        public bool Verified()
        {
            return Nickname != null || FirstName != null && LastName != null;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least 'FirstName' and 'LastName' or 'Nickname'.");

            return Wrap($"{FirstName}{MiddleName}{LastName}{Nickname}{Homepage}{Email}{Id}");
        }
    }

    public class Author : PersonBase
    {
        public override string ElemName => "author";
    }

    public class Translator : PersonBase
    {
        public override string ElemName => "translator";
    }

    public class Publisher : PersonBase
    {
        // TODO: second one-string version
        public override string ElemName => "publisher";
    }
}
