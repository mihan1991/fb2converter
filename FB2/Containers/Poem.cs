﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public class Poem : ContainerBase, IVerifiable
    {
        public Title Title { get; set; }
        public IEnumerable<Epigraph> Epigraphs { get; set; }
        public IEnumerable<Stanza> Stanzas { get; set; }
        public IEnumerable<TextAuthor> TextAuthors { get; set; }
        public Date Date { get; set; }
        public override string ElemName => "poem";

        public Poem()
        {
            Epigraphs = new List<Epigraph>();
            Stanzas = new List<Stanza>();
            TextAuthors = new List<TextAuthor>();
        }

        public bool Verified()
        {
            return Stanzas.Count() > 0;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least one 'Stanza'.");

            StringBuilder sb = new StringBuilder();
            sb.Append(Title);
            foreach (var e in Epigraphs)
                sb.Append(e);
            foreach (var s in Stanzas)
                sb.Append(s);
            foreach (var t in TextAuthors)
                sb.Append(t);
            sb.Append(Date);

            return Wrap(sb.ToString());
        }
    }
}