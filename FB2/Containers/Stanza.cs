﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public class Stanza : ContainerBase, IVerifiable
    {
        public Title Title { get; set; }
        public Subtitle Subtitle { get; set; }
        public IEnumerable<V> Vs { get; set; }
        public override string ElemName => "stanza";

        public Stanza() 
        {
            Vs = new List<V>();
        }

        public bool Verified()
        {
            return Vs.Count() > 0;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least one 'V'.");

            StringBuilder sb = new StringBuilder();
            sb.Append(Title);
            sb.Append(Subtitle);
            foreach (var v in Vs)
                sb.Append(v);

            return Wrap(sb.ToString());
        }
    }
}