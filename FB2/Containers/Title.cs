﻿using System;

namespace FB2
{
    public class Title : ContainerBase, IVerifiable
    {
        public override string ElemName => "title";

        public bool Verified()
        {
            foreach (var item in Items)
            {
                if (!(item is P || item is EmptyLine))
                    return false;
            }
            return true;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain 'Paragraph' or 'EmptyLine' elements.");

            return base.ToString();
        }
    }
}
