﻿using System;

namespace FB2
{
    public class Description : ContainerBase, IVerifiable
    {
        // TODO: add other variable nodes: SrcTitleInfo, PublishInfo, CustomInfo, Outputs 
        public TitleInfo TitleInfo { get; set; }
        public DocumentInfo DocumentInfo { get; set; }

        public override string ElemName => "description";

        public bool Verified()
        {
            if (TitleInfo != null && DocumentInfo != null)
                return TitleInfo.Verified() && DocumentInfo.Verified();
            return false;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least verified 'TitleInfo' and 'DocumentInfo'.");

            return Wrap($"{TitleInfo}{DocumentInfo}");
        }
    }
}
