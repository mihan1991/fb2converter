﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public class DocumentInfo : ContainerBase, IVerifiable
    {
        public IEnumerable<Author> Authors { get; set; }
        public ProgramUsed ProgramUsed { get; set; }
        public Date Date { get; set; }
        public IEnumerable<SrcUrl> SrcUrls { get; set; }
        public SrcOcr SrcOcr { get; set; }
        public Id Id { get; set; }
        public Version Version { get; set; }
        public History History { get; set; }
        public IEnumerable<Publisher> Publishers { get; set; }

        public override string ElemName => "document-info";

        public DocumentInfo()
        {
            Authors = new List<Author>();
            SrcUrls = new List<SrcUrl>();
            Publishers = new List<Publisher>();
        }

        public bool Verified()
        {
            return Authors.Count() > 0 && Date != null && Id != null && Version != null;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least one 'Author', 'Date' and 'Version'.");

            StringBuilder sb = new StringBuilder();
            foreach (var a in Authors)
                sb.Append(a);
            sb.Append(ProgramUsed);
            sb.Append(Date);
            foreach (var s in SrcUrls)
                sb.Append(s);
            sb.Append(SrcOcr);
            sb.Append(Id);
            sb.Append(Version);
            sb.Append(History);
            foreach (var p in Publishers)
                sb.Append(p);

            return Wrap(sb.ToString());
        }
    }
}