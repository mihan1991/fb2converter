﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB2
{
    public class TitleInfo : ContainerBase, IVerifiable
    {
        // TODO: add Sequence and Coverpage
        public IEnumerable<Genre> Genres { get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public BookTitle BookTitle { get; set; }
        public Annotation Annotation { get; set; }
        public Keywords Keywords { get; set; }
        public Date Date { get; set; }
        // public Coverpage Coverpage { get; set; }
        public Lang Lang { get; set; }
        public SrcLang SrcLang { get; set; }
        public IEnumerable<Translator> Translators { get; set; }

        // public IEnumerable<Sequence> Sequences { get; set; }

        public override string ElemName => "title-info";

        public TitleInfo()
        {
            Genres = new List<Genre>();
            Authors = new List<Author>();
            Translators = new List<Translator>();
            // Sequences = new List<Sequence>();
        }

        public bool Verified()
        {
            return Genres.Count() > 0 && Authors.Count() > 0 && BookTitle != null && Lang!=null;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least 'Genre', 'Author', 'BookTitle' and 'Lang'.");

            StringBuilder sb= new StringBuilder();
            foreach (var g in Genres)
                sb.Append(g);
            foreach (var a in Authors)
                sb.Append(a);
            sb.Append(BookTitle);
            sb.Append(Annotation);
            sb.Append(Keywords);
            sb.Append(Date);
            // sb.Append(Coverpage);
            sb.Append(Lang);
            sb.Append(SrcLang);
            foreach (var t in Translators)
                sb.Append(t);
            // foreach (var s in Sequences)
            //     sb.Append(s);

            return Wrap(sb.ToString());
        }
    }
}
