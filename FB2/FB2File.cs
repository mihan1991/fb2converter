﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace FB2
{
    public class FB2File : ContainerBase, IVerifiable
    {
        // TODO: add Stylesheet and Binary

        public string FilePath { get; set; }

        // public IEnumerable<Stylesheet> Styles {get; set;}
        public Description Description { get; set; }
        public IEnumerable<Body> Bodies { get; set; }
        // public IEnumerable<Binary> Binaries {get; set;}

        public override string ElemName => "FictionBook";

        public FB2File()
        {
            Bodies = new List<Body>();
            // Styles = new List<Styles>();
            // Binaries = new List<Binary>();

            AddAttributes(new[]
            {
                new Attribute
                {
                    Name = "xmlns",
                    Value = Constants.Xmlns
                },
                new Attribute
                {
                    Name = $"xmlns:{Constants.XlinkName}",
                    Value = Constants.Xlinkns
                }
            });
        }

        public FB2File(string path) : this()
        {
            FilePath = path;
        }


        public bool Verified()
        {
            foreach (var b in Bodies)
                if (!b.Verified())
                    return false;
            return Description.Verified() && Bodies.Count() > 0;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"{GetType().Name} can contain at least: 'Description' and one or more 'Body'.");

            StringBuilder sb = new StringBuilder();
            // foreach (var s in Styles)
            //     sb.Append(s);
            sb.Append(Description);
            foreach (var b in Bodies)
                sb.Append(b);
            // foreach (var b in Binaries)
            //     sb.Append(b);

            return Wrap(sb.ToString());
        }

        public void SaveBook()
        {
            XDocument book = new XDocument();
            string content = ToString();
#if DEBUG
            File.WriteAllText("debug.txt", content);
#endif
            XElement.Parse(content);
            book.Add(XElement.Parse(ToString()));
            book.Save(FilePath);
        }
    }
}