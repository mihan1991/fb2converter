﻿using System;
using System.Linq;

namespace FB2
{
    public class A : TextualContainerBase, IVerifiable
    {
        public override string ElemName => "a";

        public bool Verified()
        {
            return Attributes.Where(x => x.Name == $"{Constants.XlinkName}:href").Count() == 1;
        }

        public override string ToString()
        {
            if (!Verified())
                throw new Exception($"Element '{GetType().Name}' can contain at least 'href' attribute.");

            return base.ToString();
        }
    }
}