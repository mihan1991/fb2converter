﻿namespace FB2
{
    public class EmptyLine : TextualBase
    {
        public override string ElemName => "empty-line";

        public new string Text { get => ""; }

        public override string ToString() => $"<{ElemName}/>";
    }
}