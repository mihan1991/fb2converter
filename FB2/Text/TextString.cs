﻿namespace FB2
{
    public class TextString : ITextual
    {
        public string Text { get; set; }

        public override string ToString() => Text;
    }
}