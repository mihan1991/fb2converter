﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FB2;

namespace FB2Converter
{
    internal class Program
    {
        public async static Task Main(string[] args)
        {
            string inputFile = "";
            string outputFile = $"{Environment.CurrentDirectory}\\book.fb2";

            for(int i=0; i<args.Length; i++) 
            {
                switch(args[i]) 
                {
                    case "-i":
                        inputFile = args[++i];
                        break;
                    case "-o":
                        outputFile = args[++i];
                        break;
                    default:
                        break;
                }
            }

#if DEBUG
            Console.WriteLine($"\ni: {inputFile}\no: {outputFile}\n");
#endif

            if (!File.Exists(inputFile))
            {
                Console.WriteLine($"File '{inputFile}' doesn't exist.");
                PrintHelp();
                return;
            }

            try
            {
                await ConvertBook(inputFile, outputFile);
                Console.WriteLine($"Book '{outputFile}' was created.");
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
            }            
        }

        private static void PrintHelp()
        {
            Console.WriteLine(@"
FB2Converter tool

usage: FB2Converter -i <path to input .txt> -o <path to output .fb2>
");
        }

        private async static Task ConvertBook(string inputFile, string outputFile)
        {
            FB2File book = new FB2File(outputFile);

            // TODO: realize builders for FB2 elements,
            // and move parsing logic to the appropriate place.

            GenerateDefaultBookElements(book);

            await FillSectionsAsync(book, inputFile);

            book.SaveBook();
        }

        private static void GenerateDefaultBookElements(FB2File book)
        {
            book.Description = new Description
            {
                DocumentInfo = new DocumentInfo
                {
                    Authors = new List<Author>
                    {
                        new Author
                        {
                            Nickname = new Nickname{ Text = "DefaultAuthor" }
                        }
                    },
                    Date = new Date { Text = DateTime.Today.ToString("dd.MM.yyyy") },
                    Id = new Id { Text = "- no id -" },
                    Version = new FB2.Version { Text = "1.0" }
                },

                TitleInfo = new TitleInfo
                {
                    Genres = new List<Genre>
                    {
                        new Genre{Text = "prose_classic"}
                    },
                    Authors = new List<Author>
                    {
                        new Author
                        {
                            Nickname = new Nickname{ Text = "DefaultAuthor" }
                        }
                    },
                    BookTitle = new BookTitle { Text = "Book Name" },
                    Lang = new FB2.Lang { Text = "en" }
                }
            };

            book.Bodies = new List<Body>
            {
                new Body()
            };
        }

        private async static Task FillSectionsAsync(FB2File book, string inputFile)
        {
            var mainBody = book.Bodies.First();
            var mainSection = mainBody.Sections.FirstOrDefault();

            List<Section> parentLayers = new List<Section>();
            Section current = mainSection;

            var text = File.ReadAllText(inputFile).Split(Environment.NewLine);

            var tasks = new List<Task>();

            for (int i = 0; i < text.Length; i++)
            {
                if (text[i].Contains("$END$"))
                    break;

                if (text[i].StartsWith('\t'))
                {
                    int depth = text[i].Where(x => x == '\t').Count();

                    Section newSection = new Section { Autofix = true };

                    if (depth > parentLayers.Count)
                    {
                        if (current == null)
                        {
                            mainBody.Sections = new List<Section> { newSection };
                            mainSection = newSection;
                        }
                        else
                            current.AddItem(newSection);

                        parentLayers.Add(newSection);
                    }

                    else if (depth == parentLayers.Count)
                    {
                        parentLayers[parentLayers.Count - 2].AddItem(newSection);
                    }

                    else
                    {
                        while (parentLayers.Count != depth - 1)
                            parentLayers.RemoveAt(parentLayers.Count - 1);

                        parentLayers[parentLayers.Count - 1].AddItem(newSection);
                        parentLayers.Add(newSection);
                    }

                    current = newSection;

                    int e = i;
                    while (!text[e + 1].StartsWith('\t') && !text[e + 1].Contains("$END$"))
                        e++;
                    var task = new SectionBuilder(text, current, i, e).BuildAsync();
                    tasks.Add(task);
                    i = e;
                }
            }

            await Task.WhenAll(tasks);
        }
    }
}
